## Pre-requisitos
- Spring Boot 2.5.6
- Spring 5.3.12
- Spring Data 2.5.6
- Thymeleaf 3.0.12
- Tomcat embed 9.0.54
- Maven 3.6.3
- Java 8
- PostgresSQL 12

## Construir proyecto
```
mvn clean install
```

Accede a la página utilizando las URL:
- http://{host:puerto}/spring-autocomplete

