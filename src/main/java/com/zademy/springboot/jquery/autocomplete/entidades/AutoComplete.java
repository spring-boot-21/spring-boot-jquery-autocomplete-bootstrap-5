package com.zademy.springboot.jquery.autocomplete.entidades;

import java.io.Serializable;

/**
 * The Class AutoComplete.
 */
public class AutoComplete implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 4349269184028309995L;

	/** data. */
	private Integer data;

	/** value. */
	private String value;

	/**
	 * Gets the data.
	 *
	 * @return the data
	 */
	public Integer getData() {
		return data;
	}

	/**
	 * Sets the data.
	 *
	 * @param data the data to set
	 */
	public void setData(Integer data) {
		this.data = data;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Sets the value.
	 *
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AutoComplete [data=");
		builder.append(data);
		builder.append(", value=");
		builder.append(value);
		builder.append("]");
		return builder.toString();
	}

}
