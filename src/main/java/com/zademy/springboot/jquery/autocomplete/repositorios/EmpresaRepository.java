package com.zademy.springboot.jquery.autocomplete.repositorios;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.zademy.springboot.jquery.autocomplete.entidades.Empresa;

/**
 * The Interface EmpresaRepository.
 */
@Repository
public interface EmpresaRepository extends JpaRepository<Empresa, Integer>{

	/**
	 * Find by nombre like.
	 *
	 * @param dato the dato
	 * @return the list
	 */
	@Query("SELECT e FROM Empresa e WHERE e.nombre LIKE CONCAT('%',:dato,'%')")
	List<Empresa> findByNombreLike(@Param("dato") String dato);
	
}
