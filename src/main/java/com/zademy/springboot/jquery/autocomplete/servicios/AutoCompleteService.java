package com.zademy.springboot.jquery.autocomplete.servicios;

import java.util.List;

import com.zademy.springboot.jquery.autocomplete.entidades.AutoComplete;

/**
 * The Interface EmpresaService.
 */
public interface AutoCompleteService {

	/**
	 * Obtener empresa.
	 *
	 * @param dato the dato
	 * @return the list
	 */
	List<AutoComplete> obtenerEmpresas(String dato);
	
}
