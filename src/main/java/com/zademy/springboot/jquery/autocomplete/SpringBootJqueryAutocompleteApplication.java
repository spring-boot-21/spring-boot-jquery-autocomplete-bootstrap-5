package com.zademy.springboot.jquery.autocomplete;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootJqueryAutocompleteApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootJqueryAutocompleteApplication.class, args);
	}

}
