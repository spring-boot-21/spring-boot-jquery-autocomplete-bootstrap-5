package com.zademy.springboot.jquery.autocomplete.controladores;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.zademy.springboot.jquery.autocomplete.entidades.AutoComplete;
import com.zademy.springboot.jquery.autocomplete.servicios.AutoCompleteService;

/**
 * The Class AutocompleteController.
 */
@Controller
public class AutocompleteController {

	
	/** The auto complete service. */
	@Autowired
	private AutoCompleteService autoCompleteService;
	
	/**
	 * View index.
	 *
	 * @return the string
	 */
	@GetMapping("/")
	public String viewIndex() {

		return "index";
	}
	
	/**
	 * Obtener datos.
	 *
	 * @param query the query
	 * @return the string
	 */
	@GetMapping(value = "/buscar")
	@ResponseBody
	public String obtenerDatos(@RequestParam(name = "query") String query) {

		Gson gson = new Gson();

		if (query.length() > 2) {

			List<AutoComplete> autoComplete = autoCompleteService.obtenerEmpresas(query);

			return ("{\"suggestions\":" + gson.toJson(autoComplete) + "}");

		}

		return ("{\"suggestions\":}");
	}

}
