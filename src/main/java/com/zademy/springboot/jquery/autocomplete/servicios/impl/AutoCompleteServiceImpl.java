package com.zademy.springboot.jquery.autocomplete.servicios.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zademy.springboot.jquery.autocomplete.entidades.AutoComplete;
import com.zademy.springboot.jquery.autocomplete.entidades.Empresa;
import com.zademy.springboot.jquery.autocomplete.repositorios.EmpresaRepository;
import com.zademy.springboot.jquery.autocomplete.servicios.AutoCompleteService;

/**
 * The Class AutoCompleteServiceImpl.
 */
@Service
public class AutoCompleteServiceImpl implements AutoCompleteService {

	/** The empresa repository. */
	@Autowired
	private EmpresaRepository empresaRepository;

	/**
	 * Obtener empresa.
	 *
	 * @param dato the dato
	 * @return the list
	 */
	@Override
	public List<AutoComplete> obtenerEmpresas(String dato) {

		List<Empresa> empresas = empresaRepository.findByNombreLike(dato);

		List<AutoComplete> autoCompletes = new ArrayList<>();

		for (Empresa empresa : empresas) {

			AutoComplete autoComplete = new AutoComplete();
			autoComplete.setData(empresa.getIdEmpresa());
			autoComplete.setValue(empresa.getNombre());
			autoCompletes.add(autoComplete);

		}

		return autoCompletes;
	}

}
